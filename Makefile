.ONESHELL:
NS=catfood
CONCOURSE=http://18.135.132.253:8080
LOGIN=ben
PASSWORD=abc123
FLY=bin/fly -t $(NS)

bin/fly:
	http '$(CONCOURSE)/api/v1/cli?arch=amd64&platform=linux' \
		> bin/fly
	chmod +x bin/fly

login: bin/fly
	@$(FLY) status || $(FLY) login -k -u$(LOGIN) -p$(PASSWORD) -c$(CONCOURSE)

userinfo: login
	$(FLY) userinfo

pipeline: login
	$(FLY) set-pipeline -p demo -c pp.yml -l cred.yml -n

unpause:
	$(FLY) unpause-pipeline -p demo
